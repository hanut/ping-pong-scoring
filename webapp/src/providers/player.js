import * as Axios from 'axios'

export default {

  baseURL: "http://localhost:3001/api",

  create: async function (accessToken, player) {
    const headers = {Authorization: accessToken};
    try {
      let response = await Axios.post(this.baseURL + "/players", player, { headers });
      return response.data;
    } catch (error) {
      if (error.request.status === 401) {
        window.location.href = '/';
      }
      throw error;
    }
  },

  get: async function (accessToken) {
    const headers = {Authorization: accessToken};
    const filters = JSON.stringify({
      where: {
        username: {neq: "superadmin"}
      } 
    });
    try {
      let response = await Axios.get(this.baseURL + "/players?filter="+filters, { headers });
      return response.data;
    } catch (error) {
      if (error.request.status === 401) {
        window.location.href = '/';
      }
      throw error;
    }
  }

}