import * as Axios from 'axios'

export default {

  baseURL: "http://localhost:3001/api",

  top10: async function (accessToken) {
    const headers = {Authorization: accessToken};
    try {
      let response = await Axios.get(this.baseURL + "/score-cards/top10", { headers });
      return response.data;
    } catch (error) {
      if (error.request.status === 401) {
        window.location.href = '/';
      }
      throw error;
    }
  },

  create: async function (accessToken, scorecard) {
    const headers = {Authorization: accessToken};
    try {
      let response = await Axios.post(this.baseURL + "/score-cards", scorecard, { headers });
      return response.data;
    } catch (error) {
      if (error.request.status === 401) {
        window.location.href = '/';
      }
      throw error;
    }
  }

}