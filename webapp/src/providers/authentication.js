import * as Axios from 'axios'

export default {

  baseURL: "http://localhost:3001/api",

  login: async function (username, password) {
    const authData = {username: username, password: password};
    try {
      let response = await Axios.post(this.baseURL + "/players/login?include=user", authData);
      return response.data;
    } catch (error) {
      if (error.request.status === 401) {
        window.location.href = '/';
      }
      throw error;
    }
  },
  
  logout: async function(accessToken) {
    const headers = {Authorization: accessToken};
    try {
      let response = await Axios.post(this.baseURL + "/players/logout",null, {headers});
      return response.data;
    } catch (error) {
      if (error.request.status === 401) {
        window.location.href = '/';
      }
      throw error;
    }
  }

}