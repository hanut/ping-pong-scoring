import React from "react";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import NavBar from "../../components/NavBar";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import NativeSelect from "@material-ui/core/NativeSelect";
import FormHelperText from "@material-ui/core/FormHelperText";
import PlayersApi from "../../providers/player";
import ScoreboardApi from "../../providers/scoreboard";
import { navigate } from "gatsby"


const useStyles = makeStyles(theme => ({
  "@global": {
    body: {
      backgroundColor: theme.palette.common.white
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  error: {
    color: "red"
  },
  outlinedGrid: {
    borderTop: "2px dotted black",
    marginTop: "0.5rem",
    marginBottom: "0.5rem"
  }
}));

const defaultDate = (() => {
  const date = new Date();
  let year = '' + date.getFullYear();
  let month = '' + (date.getMonth() + 1);
  let day = '' + date.getDate();
  if (month.length < 2) {
    month = '0' + month;
  }
  if (day.length < 2) {
    day = '0' + day; 
  }
  return `${year}-${month}-${day}`;
})();

export default function AddPlayer() {
  const classes = useStyles();
  const title = "New Scorecard";

  const [values, setValues] = React.useState({
    players: [],
    player1: "",
    player2: "",
    set1: {
      player1: 0,
      player2: 0
    },
    set2: {
      player1: 0,
      player2: 0
    },
    set3: {
      player1: 0,
      player2: 0
    },
    date: defaultDate,
    venue: "",
    error: false
  });

  const handleChange = name => event => {
    if (name === "player1" || name === "player2") {
      const playerId = event.target.value;
      if (values.player1 === playerId || values.player2 === playerId) {
        alert("Both players can't be the same.");
        return;
      }
    }
    if (name.indexOf("set") !== -1) {
      const propVal = name.split(".");
      handleScoreChange(propVal[0], propVal[1], event.target.value);
      return;
    }
    setValues({ ...values, [name]: event.target.value });
  };

  function handleScoreChange(setName, player, score) {
    if (score < 0 || score > 11) {
      alert('Invalid points entered');
      return false;
    }
    score = parseInt(score);
    const set = values[setName];
    switch(setName) {
      case 'set1': {
        if (player === 'player1') {
          set.player1 = score;
        } else {
          set.player2 = score;
        }
        break;
      }
      case 'set2': {
        if (player === 'player1') {
          set.player1 = score;
        } else {
          set.player2 = score;
        }
        break;
      }
      case 'set3': {
        if (player === 'player1') {
          set.player1 = score;
        } else {
          set.player2 = score;
        }
        break;
      }
      default: {
        console.error("Invalid set value");
      }
    }
    setValues({...values, [setName]: set});
  }

  React.useEffect(() => {
    const token = atob(localStorage.getItem("uat"));
    PlayersApi.get(token)
      .then(result => {
        setValues({ ...values, players: result });
      })
      .catch(console.error);
  }, []);

  function save() {
    const player1Total = values.set1.player1 + values.set2.player1 + values.set3.player1;
    const player2Total = values.set1.player2 + values.set2.player2 + values.set3.player2;
    const winnerId = (player1Total > player2Total) ? values.player1 : values.player2;
    const loserId = (player1Total > player2Total) ? values.player2 : values.player1;
    const scorecard = {
      player1Id: values.player1,
      player2Id: values.player2,
      winnerId: winnerId,
      loserId: loserId,
      set1: values.set1,
      set2: values.set2,
      set3: values.set3,
      venue: values.venue,
      date: values.date
    };
    const token = atob(localStorage.getItem("uat"));
    ScoreboardApi.create(token, scorecard).then(result => {
      alert("Scorecard Added");
      navigate("/scoreboard");
    }).catch(error => alert(error.message));
  }

  return (
    <div>
      <NavBar title={title} />
      <Container component="main" maxWidth="md">
        <CssBaseline />
        <div className={classes.paper}>
          <Typography component="h1" variant="h4">
            {title}
          </Typography>
          <hr
            style={{
              marginTop: "2rem",
              marginBottom: "2rem",
              borer: "2px solid black"
            }}
          />
          <form className={classes.form} noValidate>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <NativeSelect
                  className={classes.selectEmpty}
                  value={values.player1}
                  name="player1"
                  fullWidth
                  onChange={handleChange("player1")}
                  inputProps={{ "aria-label": "player1" }}
                >
                  <option value="" disabled>
                    Player One
                  </option>
                  {values.players.map(player => (
                    <option value={player.id} key={player.id}>
                      {player.name.toUpperCase()}
                    </option>
                  ))}
                </NativeSelect>
                <FormHelperText>Player One</FormHelperText>
              </Grid>
              <Grid item xs={12} sm={6}>
                <NativeSelect
                  className={classes.selectEmpty}
                  value={values.player2}
                  name="player2"
                  fullWidth
                  onChange={handleChange("player2")}
                  inputProps={{ "aria-label": "player2" }}
                >
                  <option value="" disabled>
                    Player Two
                  </option>
                  {values.players.map(player => (
                    <option value={player.id} key={player.id}>
                      {player.name.toUpperCase()}
                    </option>
                  ))}
                </NativeSelect>
                <FormHelperText>Player Two</FormHelperText>
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  name="venue"
                  value={values.venue}
                  variant="outlined"
                  fullWidth
                  id="venue"
                  label="Venue"
                  onChange={handleChange("venue")}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="date"
                  name="date"
                  type="date"
                  defaultValue={defaultDate}
                  variant="outlined"
                  required
                  fullWidth
                  id="date"
                  label="Match Date"
                  onChange={handleChange("date")}
                />
              </Grid>
              {values.error && (
                <Grid item xs={12} sm={6} className={classes.error}>
                  {values.error}
                </Grid>
              )}
            </Grid>
            <hr style={{ marginTop: "2rem", marginBottom: "2rem" }} />
            <Typography variant="h4" component="h4">
              Scores
            </Typography>
            <br />
            { (values.player1 && values.player2) && (

            <Grid container spacing={2}>
              <Grid item xs={12} className={classes.outlinedGrid}>
                <div style={{ fontFamily: "'Times New Roman', serif" }}>
                  Set I
                </div>
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  name="s1p1"
                  value={values.set1.player1}
                  variant="outlined"
                  type="number"
                  required
                  fullWidth
                  id="s1p1"
                  label="Player1"
                  onChange={handleChange("set1.player1")}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  name="s1p2"
                  value={values.set1.player2}
                  variant="outlined"
                  type="number"
                  required
                  fullWidth
                  id="s1p2"
                  label="Player2"
                  onChange={handleChange("set1.player2")}
                />
              </Grid>
              <Grid item xs={12} className={classes.outlinedGrid}>
                <div style={{ fontFamily: "'Times New Roman', serif" }}>
                  Set II
                </div>
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  name="s2p1"
                  value={values.set2.player1}
                  variant="outlined"
                  type="number"
                  required
                  fullWidth
                  id="s2p1"
                  label="Player1"
                  onChange={handleChange("set2.player1")}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  name="s2p2"
                  value={values.set2.player2}
                  variant="outlined"
                  type="number"
                  required
                  fullWidth
                  id="s2p2"
                  label="Player2"
                  onChange={handleChange("set2.player2")}
                />
              </Grid>
              <Grid item xs={12} className={classes.outlinedGrid}>
                <div style={{ fontFamily: "'Times New Roman', serif" }}>
                  Set III
                </div>
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  name="s3p1"
                  value={values.set3.player1}
                  variant="outlined"
                  type="number"
                  required
                  fullWidth
                  id="s3p1"
                  label="Player1"
                  onChange={handleChange("set3.player1")}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  name="s3p2"
                  value={values.set3.player2}
                  variant="outlined"
                  type="number"
                  required
                  fullWidth
                  id="s3p2"
                  label="Player2"
                  onChange={handleChange("set3.player2")}
                />
              </Grid>
            </Grid>
            )}
            <Button
              size="large"
              variant="contained"
              color="secondary"
              className={classes.submit}
              onClick={save}
            >
              Save
            </Button>
          </form>
        </div>
      </Container>
    </div>
  );
}
