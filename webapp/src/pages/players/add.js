import React from "react";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import NavBar from "../../components/NavBar";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import PlayerApi from '../../providers/player';

const useStyles = makeStyles(theme => ({
  "@global": {
    body: {
      backgroundColor: theme.palette.common.white
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

export default function SignUp() {
  const classes = useStyles();
  const title = "New Player";

  const [values, setValues] = React.useState({
    name: "",
    password: '',
    dob: new Date(),
    phone: "",
    email: "",
    error: false
  });

  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value });
  };

  function save() {
    setValues({...values, error: false})
    const token = atob(localStorage.getItem('uat'));
    let player = {
      name: values.name,
      dob: values.dob,
      phone: values.phone,
      email: values.email,
      password: values.password
    };
    PlayerApi.create(token, player).then(result => {
      window.location.reload();
    }).catch(error => {
      setValues({...values, error: error.message});
    });
  }

  return (
    <div>
      <NavBar title={title} />
      <Container component="main" maxWidth="md">
        <CssBaseline />
        <div className={classes.paper}>
          <Typography component="h1" variant="h4">
            {title}
          </Typography>
          <form className={classes.form} noValidate>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="Name"
                  name="name"
                  variant="outlined"
                  value={values.name}
                  required
                  fullWidth
                  id="name"
                  label="Name"
                  autoFocus
                  onChange={handleChange("name")}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="password"
                  name="password"
                  type="password"
                  variant="outlined"
                  value={values.password}
                  required
                  fullWidth
                  id="password"
                  label="Password"
                  onChange={handleChange("password")}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="dob"
                  name="dob"
                  type="date"
                  value={values.dob}
                  variant="outlined"
                  required
                  fullWidth
                  id="dob"
                  label="Date of Birth"
                  onChange={handleChange("dob")}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="phone"
                  name="phone"
                  variant="outlined"
                  value={values.phone}
                  fullWidth
                  id="phone"
                  label="Phone Number"
                  onChange={handleChange("phone")}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="email"
                  name="email"
                  variant="outlined"
                  fullWidth
                  id="email"
                  label="Email"
                  value={values.email}
                  onChange={handleChange("email")}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                {values.error}
              </Grid>
            </Grid>
            <Button
              size="large"
              variant="contained"
              color="secondary"
              className={classes.submit}
              onClick={save}
            >
              Save
            </Button>
          </form>
        </div>
      </Container>
    </div>
  );
}
