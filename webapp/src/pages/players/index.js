import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import NavBar from "../../components/NavBar";
import PlayersApi from "../../providers/player";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from '@material-ui/core/Paper';
import AddPlayer from './add';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    overflowX: 'auto',
  },
  table: {
    minWidth: 650,
  },
}));

export default function Scoreboard() {
  const classes = useStyles();
  const title = "Scoreboard";
  const [values, setValues] = React.useState({
    players: []
  });

  React.useEffect(() => {
    const token = atob(localStorage.getItem('uat'));
    PlayersApi.get(token).then(result => {
      setValues({...values, players: result});
    }).catch(console.error);
  }, []);

  return (
    <div>
      <NavBar title={title} />
      <Container>
        <Typography variant="h2" style={{ marginTop: 2 + "rem" }}>
          {title}
        </Typography>
        <Paper className={classes.root}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell>Position</TableCell>
                <TableCell>Username</TableCell>
                <TableCell>Full Name</TableCell>
                <TableCell align="right">Phone Number</TableCell>
                <TableCell align="right">Email</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {values.players.map((player, index) => (
                <TableRow key={player.id}>
                  <TableCell component="th" scope="row">{index + 1}</TableCell>
                  <TableCell>{player.username}</TableCell>
                  <TableCell>{player.name}</TableCell>
                  <TableCell align="right">{player.phone}</TableCell>
                  <TableCell align="right">{player.email}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
        <Paper className={classes.root}>
          <AddPlayer></AddPlayer>
        </Paper>
      </Container>
    </div>
  );
}
