import React from "react";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Link from "../components/Link";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import { makeStyles } from "@material-ui/core/styles";
import AuthApi from '../providers/authentication'
import { navigate } from "gatsby"

const useStyles = makeStyles(theme => ({
  "@global": {
    body: {
      backgroundColor: theme.palette.common.white
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

export default function App() {
  const classes = useStyles();
  
  const [values, setValues] = React.useState({
    username: '',
    password: '',
    loginError: false
  });

  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value, 'loginError': false });
  };

  function login () {
    if (values.username.length < 5 || values.username.length > 12) {
      setValues({...values, loginError: true});
      return false;
    }
    if (values.password.length < 5 || values.password.length > 12) {
      setValues({...values, loginError: true});
      return false;
    }
    AuthApi.login(values.username, values.password).then(token => {
      localStorage.setItem('uat', btoa(token.id));
      localStorage.setItem('user', btoa(JSON.stringify(token.user)));
      navigate("/scoreboard", { state: { user: token.user } })
    }).catch(error => {
      if (error.response.status === 401) {
        setValues({...values, loginError: true});
      }
    });
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            error={values.loginError}
            id="username"
            variant="outlined"
            margin="normal"
            required
            fullWidth
            value={values.username}
            label="Username"
            autoComplete="username"
            autoFocus
            onChange={handleChange('username')}
            />
          <TextField
            error={values.loginError}
            id="password"
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="Password"
            type="password"
            value={values.password}
            autoComplete="current-password"
            onChange={handleChange('password')}
            />
          <Button
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={login}
          >
            Sign In
          </Button>
          <Grid container>
            <Grid item xs>
              <Link to="/forgot-password" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link to="/signup" variant="body2">
                New user?
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
}
