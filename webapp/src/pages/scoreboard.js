import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import NavBar from "../components/NavBar";
import ScoreboardApi from "../providers/scoreboard";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    overflowX: 'auto',
  },
  table: {
    minWidth: 650,
  },
}));

export default function Scoreboard() {
  const classes = useStyles();
  const title = "Scoreboard";
  const [values, setValues] = React.useState({
    scoreboard: []
  });

  React.useEffect(() => {
    ScoreboardApi.top10(atob(localStorage.getItem('uat'))).then(result => {
      setValues({...values, scoreboard: result});
    }).catch(console.error);
  }, []);

  return (
    <div>
      <NavBar title={title} />
      <Container>
        <Typography variant="h2" style={{ marginTop: 2 + "rem" }}>
          {title}
        </Typography>
        <Paper className={classes.root}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell>Position</TableCell>
                <TableCell>Player Name</TableCell>
                <TableCell align="right">Wins</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {values.scoreboard.map((row,index) => (
                <TableRow key={row._id}>
                  <TableCell component="th" scope="row">{index + 1}</TableCell>
                  <TableCell>{row.winner[0].name}</TableCell>
                  <TableCell align="right">{row.wins}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
      </Container>
    </div>
  );
}
