import React from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Link from '../components/Link';

export default function App() {
  return (
    <Container maxWidth="sm">
      <Box my={4}>
        <Typography variant="body1" gutterBottom>
          Follow the instructions to reset your password
        </Typography>
        <Link to="/">Go Back</Link>
      </Box>
    </Container>
  );
}
