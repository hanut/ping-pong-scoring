'use strict';

module.exports = function enableAuthentication(server) {
  // enable authentication
  server.enableAuth();

  server.models.Player.findOrCreate({
  	username: "superadmin"
  },{
  	username: "superadmin",
  	password: "qwe123",
  	phone: "+917016907360",
  	name: "Super Admin",
  	dob: "1989-07-02T18:30:00.000Z",
  	isEmailVerified: true
  }).then(player => {
  	console.log("Admin username:", player[0].username);
  	console.log("Admin password:", "qwe123");
  }).catch(error => {
  	console.log(error);
  	process.exit(1);
  });
};
