'use strict';
const DisableRemoteMethods = require('./model-helpers/disable-all-remotes.js');

module.exports = function(Player) {
	
	DisableRemoteMethods(Player, ["find", "findOne", "findById", "create", "patchAttributes", "deleteById"]);

	Player.disableRemoteMethodByName('prototype.__get__accessTokens');
	Player.disableRemoteMethodByName('prototype.__create__accessTokens');
	Player.disableRemoteMethodByName('prototype.__delete__accessTokens');
	Player.disableRemoteMethodByName('prototype.__findById__accessTokens');
	Player.disableRemoteMethodByName('prototype.__updateById__accessTokens');
	Player.disableRemoteMethodByName('prototype.__destroyById__accessTokens');
	Player.disableRemoteMethodByName('prototype.__count__accessTokens');

};
