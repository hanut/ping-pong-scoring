'use strict';

const RemoteMethods = [
	'create',
	'upsert',
	'deleteById',
	'updateAll',
	'updateAttributes',
	'patchAttributes',
	'createChangeStream',
	'findOne',
	'find',
	'findById',
	'count',
	'exists',
	'replace',
	'replaceById',
	'upsertWithWhere',
	'replaceOrCreate'
]

module.exports = function (Model, exclusions) {
	RemoteMethods.forEach(method => {
		if (exclusions.indexOf(method) !== -1) {
			return;
		}
		if (!!Model.prototype[method]) {
            Model.disableRemoteMethodByName('prototype.' + method);
        } else {
            Model.disableRemoteMethodByName(method);
        }
	});
}