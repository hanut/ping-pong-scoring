"use strict";

module.exports = function(Scorecard) {
  
  Scorecard.topten = function (callback) {
    const collection = Scorecard.getDataSource().connector.collection(Scorecard.modelName);
    collection.aggregate([
      {
        $group: {
          _id: "$winnerId",
          wins: {
            $sum: 1
          }
        }
      },
      {
        $lookup: {
          from: "Player",
          localField: "_id",
          foreignField: "_id",
          as: "winner"
        }
      },
      {
        $sort : { wins : -1 }
      },
      { $limit: 10 }
    ], function(err, data) {
      if (err) {
        return callback(err);
      }
      data.toArray().then(scoreboard => callback(null, scoreboard)).catch(error => callback(error));
    });
  }
};
